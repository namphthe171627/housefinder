<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login Form</title>
        <link
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            rel="stylesheet"
            />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css"
            />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <style>
            .main-container {
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                background-color: #f8f9fa;
            }
            .login-container {
                width: 400px;
                background: #fff;
                padding: 2rem;
                border-radius: 8px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                text-align: center;
            }
            .login-container h2 {
                margin-bottom: 1.5rem;
            }
            .btn-google {
                background: #fff;
                border: 1px solid #ddd;
                color: #555;
            }
            .btn-google img {
                width: 20px;
                margin-right: 8px;
            }
            .or-separator {
                margin: 1.5rem 0;
                font-size: 1rem;
                color: #888;
            }
            .form-control {
                margin-bottom: 1rem;
            }
            .btn-login {
                background: #000;
                color: #fff;
            }
            .links {
                margin-top: 1rem;
                font-size: 0.9rem;
            }
            .links a {
                color: #007bff;
            }
        </style>
    </head>
    <body>
        <div class="logo d-flex justify-content-center align-items-center" style="width: 150px; height: 60px;">
            <a style="font-size: 22px; text-decoration: none; color: black;" href="../../home.jsp">
                <span class="text-warning">House</span>Finder
            </a>
        </div>
        <div class="main-container">
            <div class="login-container">
                <h2>Đăng nhập</h2>
                <button class="btn btn-google btn-block">
                    <img src="https://img.icons8.com/color/16/000000/google-logo.png" alt="Google logo">Đăng nhập với google
                </button>
                <div class="or-separator">hoặc</div>
                <form>
                    <input type="email" class="form-control" placeholder="Email" required>
                    <input type="password" class="form-control" placeholder="Mật khẩu" required>
                    <input id="checkbox" type="checkbox" class="form-check-input"/>
                    <label for="checkbox" class="form-label">Ghi nhớ mật khẩu</label>
                    <button type="submit" class="btn btn-login btn-block">Đăng nhập</button>
                </form>
                <div class="links">
                    <a href="#">Quên mật khẩu</a><br>
                    Chưa có tài khoản? <a href="register.jsp">Đăng ký</a>
                </div>
            </div>
        </div>
        <!-- Bootstrap JS and dependencies -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    </body>
</html>
