
package utils;

import jakarta.servlet.http.HttpServletRequest;

public class GetParam {
     
    private GetParam() {
        throw new UnsupportedOperationException("Utility class");
    }

    public static String getStringParam(
            HttpServletRequest request, 
            String field, 
            String label, 
            int max, 
            int min, 
            String defaultValue) {
        String value = (String) request.getParameter(field);
        
        String errorResponse = field + "Error";
        if (value == null || value.trim().isEmpty()) {
            if(defaultValue == null) {
                request.setAttribute(errorResponse, label + "is required");
                return null;
            }
            
            return defaultValue;
        }
        
        if(value.trim().length() > max) {
            request.setAttribute(errorResponse, label + " is less than or equal " + max + " character(s)");
            return null;
        }
        
        if(value.trim().length() < min) {
            request.setAttribute(errorResponse, label + " is more than or equal " + min + " character(s)");
            return null;
        }
        
        return value;
    }
    
    
     public static Object getClientAttribute(HttpServletRequest request, String field, Object defaultValue) {
        Object value = request.getAttribute(field);
        if (value == null) {
            return defaultValue;
        }

        return value;
    }

    public static Object getClientParams(HttpServletRequest request, String field, Object defaultValue) {
        Object value = request.getParameter(field);
        if (value == null) {
            return defaultValue;
        }

        return value;
    }
}
